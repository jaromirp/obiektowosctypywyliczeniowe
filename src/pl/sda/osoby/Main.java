package pl.sda.osoby;

public class Main {
    public static void main(String[] args) {
        Osoba roman = new Osoba ("Roman", "Kowlaski", 25);
        roman.przedstawSie();

        Student olek = new Student ("Aleksander", "Nowak", 21, 111);

        olek.przedstawSie();

        System.out.println(olek);              //to znaczy samo co poniżej
        System.out.println(olek.toString());   // to znaczy samo co powyżej

        Student bolek = new Student("Bolesłąw", "Zieliński", 35, 111);

        System.out.println(olek.equals(bolek)); //czy oba mają ten sam indeks w chmurce
    }
}
