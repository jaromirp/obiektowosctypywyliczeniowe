package pl.sda.osoby;

public class Student extends Osoba {
    private long numerIndeksu;

    public Student(String imie, String nazwisko, int wiek, long numerIndeksu) {
        super(imie, nazwisko, wiek); // musi być jako pierwsza linia Konstruktora
        this.numerIndeksu = numerIndeksu;
    }

    @Override
    public String toString() {
        //return super.toString();
        String opis = String.format("%s %s, nr albumu: %d", imie, nazwisko, numerIndeksu);
        return opis;
    }

    @Override
    public void przedstawSie() {
        //super.przedstawSie(); //Usuwamy było przed Override Ctrl + O
        System.out.println(String.format("Cześć, jestem %s i studiuje prawo", imie));

    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj); ta wartość byłą
        if (obj instanceof Student) {
            Student ten = (Student) obj;  //jeśli rzutujemy w góre to jest w nawiasach
            if (this.numerIndeksu == ten.numerIndeksu) {
                return true;

            }

        }
        return false;

    }


}


