package pl.sda.samochody;

public class Kabriolet extends Samochod {
    boolean dach; //False

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }


 //   @Override
 //   public String toString() {
 //       //return super.toString(); to było wcześniej
 //       return String.format("%s samochód marki %s rocznik %d z rozsuwanym dachem");
 //   }

    public void schowajDach() {
        if (dach){
            System.out.println("Dach juz jest schowany");
        }else {
            dach=true;
            System.out.println("Chowam dach");
        }

    }


    @Override
    public void przyspiesz() {
        //super.przyspiesz();
        if (predkoscSamochodu<180){
            predkoscSamochodu+=20;
            System.out.println(predkoscSamochodu);

        }
    }

    public boolean czydachSchowany() {
        return dach;

    }

    @Override
    public String toString() {
        String s = super.toString() + " z rozsuwanym dachem";
        return s;

    }

}


