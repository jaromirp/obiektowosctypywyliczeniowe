package pl.sda.samochody;

public class Samochod {

    String rodzajSamochodu;
    int predkoscSamochodu = 0;
    //int predkoscMax = 120;
    String kolor, marka;
    int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    boolean swiatla; // domyślnie False

//    public Samochod(String rodzajSamochodu, int predkoscSamochodu, int predkoscMax) {
//        this.rodzajSamochodu = rodzajSamochodu;
//        this.predkoscSamochodu = predkoscSamochodu; //km/h
//        //this.predkoscMax = predkoscMax;  //km/h
//    }

    public void przyspiesz() {
        if (predkoscSamochodu < 120) {
            predkoscSamochodu += 10;
            System.out.printf("Przyspieszam do %d km/h\n", predkoscSamochodu);
        }
        //for (int i = 0; i < 120; i = i + 10) {
        //    System.out.printf("Przyspieszam do prędkości %d ", predkoscSamochodu);

    }

    public void wlaczSwiatla() {
        swiatla = true;
    }

    public boolean czySwiatlaWlaczone() {
        return swiatla;
    }

    @Override
    public String toString() {
        return String.format("%s samochód marki %s rocznik %d", kolor, marka, rocznik);

    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod){
            Samochod ten = (Samochod) obj;
            if (this.kolor.equals(ten.kolor)){
                if (this.marka.equals(ten.marka)){
                    if (this.rocznik==ten.rocznik){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}



